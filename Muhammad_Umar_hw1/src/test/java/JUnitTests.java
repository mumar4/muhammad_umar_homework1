
import static junit.framework.TestCase.assertEquals;

import org.json.JSONException;
import org.junit.Test;


import java.io.IOException;


public class JUnitTests {

    @Test
    public void testUserOpName() throws IOException, JSONException {

        UserOp u = new UserOp();
        u.execute("{viewer {email login url}}","https://api.github.com/graphql");
        String result = "mumar4";

        assertEquals(u.returnUser().getLogin(), result);

    }

    @Test
    public void testUserOpUrl() throws IOException, JSONException {

        UserOp u = new UserOp();
        u.execute("{viewer {email login url}}","https://api.github.com/graphql");
        String result = "https://github.com/mumar4";

        assertEquals(u.returnUser().getUrl(), result);

    }

    @Test
    public void testUserOpEmail() throws IOException, JSONException {

        UserOp u = new UserOp();
        u.execute("{viewer {email login url}}","https://api.github.com/graphql");
        String result = "";

        assertEquals(u.returnUser().getEmail(), result);

    }

    @Test
    public void testRepoOpSize() throws IOException, JSONException {

        RepoOp r = new RepoOp();
        r.execute("{viewer{login repositories(last: 4) { nodes { name createdAt url languages(first:1) { nodes { name } } }}}}","https://api.github.com/graphql");
        int result = 4;

        assertEquals(r.getNumRepos(), result);

    }

    @Test
    public void testRepoOpName() throws IOException, JSONException {

        RepoOp r = new RepoOp();
        r.execute("{viewer{login repositories(last: 4) { nodes { name createdAt url languages(first:1) { nodes { name } } }}}}","https://api.github.com/graphql");
        String result = "342Project4";

        assertEquals(r.returnRepos().get(0).getName(), result);

    }

}
