import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.stream.Collectors;

//EXECUTE REPO OPERATION, STORING REPO DATA
public class RepoOp implements Operation{

    //STORE REPO DATA
    public ArrayList <Repository> repos1 = new ArrayList<Repository>();

    @Override
    public void execute(String query, String baseUrl) throws IOException, JSONException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost httpUriRequest = new HttpPost(baseUrl);
        httpUriRequest.addHeader("Authorization", "Bearer 25dc880d1685f757816b4ca31eb01bdf3ad3db54");
        httpUriRequest.addHeader("Accept", "application/json");

        httpUriRequest.setEntity(new StringEntity("{\"query\":\"" + query + "\"}"));

        HttpResponse response = (HttpResponse) client.execute(httpUriRequest);
        System.out.println("Response:" + response);

        if(response == null){
            System.out.println("Response entity is null");
        }
        else {
            String result = new BufferedReader(new InputStreamReader(response.getEntity().getContent())).lines().collect(Collectors.joining("\n"));
            System.out.println(result);

            ArrayList<Repository> repos = getLatestFourRepositories(result);

        }
    }

    //USED FOR TESTING TO SEE IF CORRECT NUMBER OF REPOS WERE RETRIEVED
    public int getNumRepos(){
        return repos1.size();
    }

    //USED FOR TESTING
    public ArrayList<Repository> returnRepos(){
        return repos1;
    }

    //DRILLS DOWN THE JSON TO RETRIEVE THE DATA FROM EACH NODE
    private ArrayList<Repository>  getLatestFourRepositories(String result1) throws JSONException {
        JSONObject json = new JSONObject (result1);
        JSONArray nodes = json.getJSONObject("data")
                .getJSONObject("viewer")
                .getJSONObject("repositories")
                .getJSONArray("nodes");

        ArrayList<Repository> repos = new ArrayList<Repository>();
        //RUN THROUGH EACH NODE, CREATE REPO OBJECT AND PUSH TO ARRAYLIST
        for (int i = 0; i < nodes.length(); ++i) {
            JSONObject node = nodes.getJSONObject(i);
            String name = node.getString("name");
            String createdAt = node.getString("createdAt");
            String url = node.getString("url");
            JSONArray languages = node.getJSONObject("languages")
                    .getJSONArray("nodes");
            String language = languages.getJSONObject(0).getString("name");

            Repository r = new Repository(name, createdAt, url, language);
            repos.add(r);
            repos1.add(r);
            r.displayInfo();
        }

        return repos;
    }

}
