import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.stream.Collectors;

//EXECUTES USER OPERATION
public class UserOp  implements Operation{

    public User u;

    @Override
    public void execute(String query, String baseUrl) throws IOException, JSONException {

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost httpUriRequest = new HttpPost(baseUrl);
        httpUriRequest.addHeader("Authorization", "Bearer 25dc880d1685f757816b4ca31eb01bdf3ad3db54");
        httpUriRequest.addHeader("Accept", "application/json");

        httpUriRequest.setEntity(new StringEntity("{\"query\":\"" + query + "\"}"));

        HttpResponse response = (HttpResponse) client.execute(httpUriRequest);
        System.out.println("Response:" + response);

        if(response == null){
            System.out.println("Response entity is null");
        }
        else {
            String result = new BufferedReader(new InputStreamReader(response.getEntity().getContent())).lines().collect(Collectors.joining("\n"));
            System.out.println(result);

            User user = createUser(result);
            u = user;
            user.displayInfo();

        }

    }

    public User returnUser(){
        return u;
    }

    //CREATE AND RETURN CURRENT USER
    private User createUser(String result)throws JSONException {
        JSONObject json = new JSONObject(result);
        String login = json.getJSONObject("data")
                .getJSONObject("viewer").getString("login");

        String email = json.getJSONObject("data")
                .getJSONObject("viewer").getString("email");

        String url = json.getJSONObject("data")
                .getJSONObject("viewer").getString("url");

        User user = new User(email, login, url);

        return user;
    }
}
