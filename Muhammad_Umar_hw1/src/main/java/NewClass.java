
import org.json.JSONException;

import java.io.IOException;

import java.util.Scanner;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//THE MAIN CLASS THAT EXECUTES THE COMMANDS
public class NewClass {

    //LOGGING VARIABLE
    private static final Logger log = LoggerFactory.getLogger(NewClass.class);

    public static void main(String[] args) throws IOException, JSONException {

        log.info("info message");
        String BASE_GHQL_URL = "https://api.github.com/graphql";

        Operation operation;
        Scanner reader = KeyboardScanner.getKeyboardScanner();
        log.warn("ONLY LOWER CASE COMMANDS ACCEPTED");
        System.out.println("Pick a command");
        System.out.println("u - Save and Display User Info");
        System.out.println("r - Save and Display Latest 4 Repositories");


        String input = reader.nextLine();

        if(input.equals("u")){
            String temp="{viewer {email login url}}";
            operation = new UserOp();
            //EXECUTING THE SAVE AND DISPLAY USER INFO OPERATION
            operation.execute(temp,BASE_GHQL_URL);
        }
        if(input.equals("r")){
            String temp = "{viewer{login repositories(last: 4) { nodes { name createdAt url languages(first:1) { nodes { name } } }}}}";
            operation = new RepoOp();
            //EXECUTING THE SAVE AND DISPLAY LATEST FOUR REPOSITORIES OPERATION
            operation.execute(temp,BASE_GHQL_URL);
        }
        else{
            log.error("INCORRECT INPUT");
        }

    }

}

