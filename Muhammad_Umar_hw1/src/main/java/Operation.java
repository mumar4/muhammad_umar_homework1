import org.json.JSONException;

import java.io.IOException;

//INTERFACE USED FOR COMMAND DESIGN PATTERN
public interface Operation {

    void execute(String query, String baseUrl) throws IOException, JSONException;
}
