//ENCAPSULATES A SINGLE USER
public class User {

    private String email;
    private String login;
    private String url;


    public User(String email, String login, String url) {
        this.email = email;
        this.login = login;
        this.url = url;
    }

    public void displayInfo(){
        System.out.println("-------User Info--------");
        System.out.println("Login: "+ this.login);
        System.out.println("Email: "+this.email);
        System.out.println("URL: "+this.url+"\n");
    }

    public String getEmail() {
        return email;
    }

    public String getLogin() {
        return login;
    }

    public String getUrl() {
        return url;
    }
}

