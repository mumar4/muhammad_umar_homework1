//ENCAPSULATES A SINGLE REPOSITORY
public class Repository {
    private String name;
    private String createdAt;
    private String url;
    private String language;


    public Repository(String name, String createdAt, String url, String language) {
        this.name = name;
        this.createdAt = createdAt;
        this.url = url;
        this.language = language;
    }

    public void displayInfo(){
        System.out.println("-------Repositories Info--------");
        System.out.println("Name: "+ this.name);
        System.out.println("createdAt: "+this.createdAt);
        System.out.println("URL: "+this.url);
        System.out.println("Language: "+this.language+"\n");

    }

    public String getName() {
        return name;
    }

    public String getCreatedAt() {
        return createdAt;
    }
}
