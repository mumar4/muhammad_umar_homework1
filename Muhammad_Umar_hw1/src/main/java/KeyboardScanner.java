import java.util.Scanner;

//CLASS ENCAPSULATED SCANNER AND SYSTEM.IN
//USING SINGLETON DESIGN PATTERN FOR USER INPUT
public class KeyboardScanner {

    //define scanner as static
    private static KeyboardScanner s = null;
    private static Scanner keyboard;

    //----------------------------------------------------
    //----------------------------------------------------
    private KeyboardScanner() {
        keyboard = new Scanner(System.in);
    }

    //----------------------------------------------------
    //----------------------------------------------------
    static Scanner getKeyboardScanner() {
        if(s== null) {
            s  = new KeyboardScanner();
        }
        return keyboard;
    }



}
