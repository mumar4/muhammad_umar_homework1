----**Design**----------------------------------------------------------------

**Functionality** 
There are two main functionalities of the project. The first is gathering the current user info, 
which includes the email, login, and URL of the Github and the second is gathering the info on 
the latest four repositories in the Github. For each repository, the name of the repo, create date, 
URL and languages used in the project are stored. 

**Design Patterns**
To implement the two functionalities I utilized the singleton and command design patterns. The 
singleton design pattern is useful because an instance of a class can be used multiple times 
throughout the program. I used the singleton design pattern by encapsulating the java scanner class
so anytime user input is required the instance of the class can be called. While this is not extremely
useful, it does help to reduce redundant code. The second design pattern was command. Command takes the
user input and encapsulates it as an object to be executed. This design uses an interface whose execute
method can be used to fulfill any command given, in my case gathering user info and gathering info
on the latest 4 repositories. This design offers more flexibility and makes creating new commands
efficient and easy. 

**JUNIT**
Multiple junit tests were created to ensure correct input when retrieving info from GraphQL API.
The tests are located in the test folder.

**Logging**
Logback was the tool used for logging. The program includes several logs of which are messages, 
warnings, and errors. The logs are added in the code to ensure user input is correct, and give 
feedback on if things are working properly.

**Gradle**
Gradle was utilized for building and dependencies.

**The different files are:**

Class NewClass - The main class that takes in user input and executes the command accordingly
Interface Operation - Interface with execute method used for command pattern
Class RepoOp implements Operation - Executes repository info retrieving operation
Class UserOp implements Operation - Executes user info retrieving operation
Class Repository - Encapsulates repo data
Class User - Encapsulates user data

----**Instructions**----------------------------------------------------------------
1) Build and run the program
2) Type 'u' to run user data query or type 'r' to run repo data query
3) Program should run and retrieve the specified data



